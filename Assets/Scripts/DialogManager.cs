﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogManager : MonoBehaviour
{
    public static DialogManager instance;

    public Text dialog_label;
    private bool active = false;
    private float currentCountdown;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this);
        }
    }

    void Start()
    {
        currentCountdown = 1;
    }


    void Update()
    {
        if(active)
        {
            if (currentCountdown < 0)
            {
                active = false;
                currentCountdown = 1;
                dialog_label.text = "";
            }
            currentCountdown -= Time.deltaTime;
        }
    }

    public void ShowDialog(string text, float duration)
    {
        currentCountdown = duration;
        dialog_label.text = text;
        active = true;
    }
}

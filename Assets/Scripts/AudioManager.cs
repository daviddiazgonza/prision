﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;


public class AudioManager : MonoBehaviour
{
	// Audio players components.
	public AudioSource EffectsSource;
	public AudioSource MusicSource;

	public AudioClip doorClip;
	public AudioClip plankClip;
	public AudioClip craneClip;
	public AudioClip keyClip;
	public AudioClip lockedDoorClip;
	public AudioClip gateClip;
	public AudioClip stoneDoorClip;
	public AudioClip hatchClip;
	public AudioClip paperClip;
	public AudioClip metalDoorClip;
	public AudioClip laserChargeClip;
	public AudioClip electricityClip;
	public AudioClip laserhootClip;
	public AudioClip punchClip;


	// Random pitch adjustment range.
	public float LowPitchRange = .95f;
	public float HighPitchRange = 1.05f;

	// Singleton instance.
	public static AudioManager Instance = null;

	// Initialize the singleton instance.
	private void Awake()
	{
		// If there is not already an instance of SoundManager, set it to this.
		if (Instance == null)
		{
			Instance = this;
		}
		//If an instance already exists, destroy whatever this object is to enforce the singleton.
		else if (Instance != this)
		{
			Destroy(gameObject);
		}

		//Set SoundManager to DontDestroyOnLoad so that it won't be destroyed when reloading our scene.
		DontDestroyOnLoad(gameObject);
	}

	// Play a single clip through the sound effects source.
	public void Play(string clip)
	{
		switch (clip)
		{
			case "door":
				EffectsSource.clip = doorClip;
				break;
			case "plank":
				EffectsSource.clip = plankClip;
				break;
			case "crane":
				EffectsSource.clip = craneClip;
				break;
			case "key":
				EffectsSource.clip = keyClip;
				break;
			case "locked_door":
				EffectsSource.clip = lockedDoorClip;
				break;
			case "gate":
				EffectsSource.clip = gateClip;
				break;
			case "stone_door":
				EffectsSource.clip = stoneDoorClip;
				break;
			case "hatch":
				EffectsSource.clip = hatchClip;
				break;
			case "paper":
				EffectsSource.clip = paperClip;
				break;
			case "metal_door":
				EffectsSource.clip = metalDoorClip;
				break;
			case "electricity":
				EffectsSource.clip = electricityClip;
				break;
			case "laser_shot":
				EffectsSource.clip = laserhootClip;
				break;
			case "punch":
				EffectsSource.clip = punchClip;
				break;
		}

		EffectsSource.Play();
	}

    public void Pause()
    {
		EffectsSource.Pause();
	}

    // Play a single clip through the music source.
    public void PlayMusic()
	{
		MusicSource.clip = laserChargeClip;
		MusicSource.Play();
	}

	// Play a random clip from an array, and randomize the pitch slightly.
	public void RandomSoundEffect(params AudioClip[] clips)
	{
		int randomIndex = Random.Range(0, clips.Length);
		float randomPitch = Random.Range(LowPitchRange, HighPitchRange);

		EffectsSource.pitch = randomPitch;
		EffectsSource.clip = clips[randomIndex];
		EffectsSource.Play();
	}

	public void PauseMusic()
	{
		MusicSource.Pause();
	}

}


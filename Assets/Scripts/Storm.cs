﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class Storm : MonoBehaviour
{
    private float timelapse = 6;
    private float timelapse_2 = 0.2f;
    public AudioClip[] stormSounds;
    public AudioSource stormAudioSource;
    public GameObject player;

    private bool soundIsBeingPlayed = false;

    public static int stormValue = 1;

    // Update is called once per frame
    void Update()
    {
        timelapse -= Time.deltaTime;
        if(timelapse <= 0)
        {
            stormValue = 10;
            player.GetComponent<Camera>().backgroundColor = Color.white;
            if (!soundIsBeingPlayed)
            {
                soundIsBeingPlayed = true;
                stormAudioSource.PlayOneShot(stormSounds[Random.Range(0, stormSounds.Length)]);
            }
            
            timelapse_2 -= Time.deltaTime;
            if(timelapse_2 <= 0)
            {
                stormValue = 1;
                player.GetComponent<Camera>().backgroundColor = Color.black;
                timelapse_2 = 0.2f;
                timelapse = Random.Range(10, 23);
                soundIsBeingPlayed = false;
            }
        }
    }
}

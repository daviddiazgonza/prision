﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Elevator : MonoBehaviour
{

    private bool finish = false;
    private bool start = false;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.K))
        {
            start = true;
        }

        if (start)
        {
            if (!finish)
            {
                transform.Translate(Vector3.up * Time.deltaTime * 4);
                if (transform.position.y >= 180)
                {
                    finish = true;
                    start = false;
                }
            }
        }
    }
}


//INITIAL 49.63
//FINAL 181.59

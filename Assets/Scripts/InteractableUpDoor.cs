﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class InteractableUpDoor : InteractableObject
{
    public Vector3 finalPosition;
    public GameObject gate;

    private void Start()
    {
        message = "OPEN DOOR";
    }

    public override void Interact()
    {
        AudioManager.Instance.Play("stone_door");
        this.transform.GetComponent<Animator>().SetTrigger("Activate");
        gate.transform.DOLocalMove(finalPosition, 3);
        DisableObject();
    }
}

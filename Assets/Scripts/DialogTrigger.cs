﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogTrigger : MonoBehaviour
{

    private int index;
    public bool destroyAfterTrigger;
    public float duration;
    public string[] dialogs;
    // Start is called before the first frame update
    void Start()
    {
        index = 0;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            DialogManager.instance.ShowDialog(dialogs[index], duration);
            if (index == dialogs.Length - 1)
            {
                if (destroyAfterTrigger)
                {
                    Destroy(this.gameObject);
                }
            }

            if (index < dialogs.Length - 1)
            {
                index++;
            }

            
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableBoards : InteractableObject
{
    // Start is called before the first frame update
    void Start()
    {
        message = "REMOVE PLANK";
    }

    public override void Interact()
    {
        AudioManager.Instance.Play("plank");
        this.gameObject.AddComponent<Rigidbody>();
        this.gameObject.GetComponent<Rigidbody>().AddForce(-Vector3.forward * 50);
        DisableObject();
    }
}

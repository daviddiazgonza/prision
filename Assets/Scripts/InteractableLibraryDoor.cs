﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;


public class InteractableLibraryDoor : InteractableObject
{
    public Transform rightDoor;
    public Transform leftDoor;
    private float speed = 3;

    private void Start()
    {
        message = "INTERACT";
    }

    public override void Interact()
    {
        AudioManager.Instance.Play("stone_door");
        leftDoor.DOMove(new Vector3(175.02f, 202.046f, 472.47f), speed);
        rightDoor.DOMove(new Vector3(167.83f, 202.046f, 472.47f), speed);
        DisableObject();
    }
}

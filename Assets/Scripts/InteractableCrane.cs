﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableCrane : InteractableObject
{
    public GameObject crane;

    private void Start()
    {
        message = "ACTIVATE CRANE";
    }

    public override void Interact()
    {
        AudioManager.Instance.Play("crane");
        this.transform.GetComponent<Animator>().SetTrigger("Activate");
        crane.transform.GetComponent<Animator>().SetTrigger("Activate");

        DisableObject();

    }

    public void EndAnimation()
    {
        AudioManager.Instance.Pause();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableLetter : InteractableObject
{
    public GameObject letter;
    private bool reading;
    private bool canInteract;
    private void Start()
    {
        canInteract = false;
        reading = false;
        message = "READ LETTER";
    }

    private void Update()
    {
        if ((Input.GetKeyUp(KeyCode.E) || Input.GetButtonUp("A Button")) && reading)
        {
            canInteract = true;
        }

        if ((Input.GetKeyDown(KeyCode.E) || Input.GetButtonDown("A Button")) && reading && canInteract)
        {
            letter.SetActive(false);
            this.transform.tag = "interactable";
            reading = false;
            canInteract = false;
            GameManager.instance.transform.GetComponent<InitialTrainScript>().postRead = true;
        }
    }

    public override void Interact()
    {
        if (!reading)
        {
            AudioManager.Instance.Play("paper");
            letter.SetActive(true);
            DisableObject();
            reading = true;
        }
    }
}

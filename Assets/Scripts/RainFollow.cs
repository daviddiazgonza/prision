﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RainFollow : MonoBehaviour
{


    public bool isFollowingCharacter = false;

    public Transform player;


    //When Following Player
    public float followSpeed = 4f;
    public float camera_distance_x = 0f;
    public float camera_distance_z = 0f;

    private float xDistance;
    private float zDistance;

    private float currentXDistance;
    private float currentZDistance;

    private float playerHeight = 0;

    void Start()
    {
        playerHeight = player.transform.position.y + 10;
    }

    private void Update()
    {
        if (isFollowingCharacter)
        {
            xDistance = player.position.x + camera_distance_x;
            zDistance = player.position.z + -camera_distance_z;
            currentXDistance = Mathf.Lerp(this.transform.position.x, xDistance, followSpeed * Time.deltaTime);
            currentZDistance = Mathf.Lerp(this.transform.position.z, zDistance, followSpeed * Time.deltaTime);
            this.transform.position = new Vector3(currentXDistance, player.transform.position.y + 30, currentZDistance);
        }
    }

    public void FollowPlayer()
    {
        isFollowingCharacter = true;
    }

    public void UnfollowPlayer()
    {
        isFollowingCharacter = false;
    }
}

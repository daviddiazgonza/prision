﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class InteractableDiningRoomDoor : InteractableObject
{
    public Transform door;
    public Transform otherStwicher;
    private float speed = 3;

    private void Start()
    {
        message = "INTERACT";
    }

    public override void Interact()
    {
        AudioManager.Instance.Play("stone_door");
        door.DOMove(new Vector3(191.51f, 212.29f, 480.04f), speed);
        DisableObject();
        otherStwicher.GetComponent< InteractableDiningRoomDoor>().DisableObject();
    }
}

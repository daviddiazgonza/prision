﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OutilerScriptApplier : MonoBehaviour
{
    private Color outlineColor = new Color(1,0.7238f,0.5098f);
    public GameObject[] objectList;
    // Start is called before the first frame update
    void Awake()
    {
        foreach(GameObject obj in objectList)
        {
            Outline outlineComponent = obj.AddComponent<Outline>();
            outlineComponent.OutlineColor = outlineColor;
            outlineComponent.OutlineMode = Outline.Mode.OutlineVisible;
            outlineComponent.OutlineWidth = 7;
        }
    }
}

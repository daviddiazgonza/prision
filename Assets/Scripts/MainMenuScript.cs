﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using DG.Tweening;
using UnityEngine.UI;

public class MainMenuScript : MonoBehaviour
{
    public Transform startText;
    private bool alreadyPushed = false;
    public GameObject title;

    void Start()
    {
        startText.DOScale(1.02f, 1f).SetLoops(-1, LoopType.Yoyo);
    }

    void Update()
    {
        if (Input.anyKeyDown && !alreadyPushed)
        {
            //title.GetComponent<RectTransform>().DOMoveY(180, 3);
            LoadScene();
            //this.transform.DOMove(new Vector3(0, 154.2f, -650), 4).OnComplete(LoadScene);
            startText.gameObject.SetActive(false);
            alreadyPushed = true;
            
        }
    }

    public void LoadScene() 
    { 
        SceneManager.LoadScene(1); 
    }
}

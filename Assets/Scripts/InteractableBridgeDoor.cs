﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableBridgeDoor : InteractableObject
{
    public GameObject gate;
    public GameObject gate2;

    private void Start()
    {
        message = "ACTIVATE MECHANISM";
    }

    public override void Interact()
    {
        AudioManager.Instance.Play("gate");
        this.transform.GetComponent<Animator>().SetTrigger("Activate");
        if ( gate != null) gate.transform.GetComponent<Animator>().SetTrigger("Activate");
        if (gate2 != null)  gate2.transform.GetComponent<Animator>().SetTrigger("Activate");

        DisableObject();

    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AreaLoader : MonoBehaviour
{
    public List<GameObject> lastArea;
    public List<GameObject> nextArea;
    // Start is called before the first frame update

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            foreach(GameObject obj in lastArea)
            {
                obj.SetActive(false);
            }
            foreach (GameObject obj in nextArea)
            {
                obj.SetActive(true);
            }
        }
    }
}

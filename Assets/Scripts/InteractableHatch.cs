﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class InteractableHatch : InteractableObject
{
    public Transform target;
    public GameObject hashCollider;
    public Vector3 finalRotation;

    private void Start()
    {
        message = "ACTIVATE";
    }

    public override void Interact()
    {
        AudioManager.Instance.Play("hatch");
        target.DORotate(finalRotation, 0.33f, RotateMode.Fast);
        hashCollider.SetActive(true);
        DisableObject();
    }
}

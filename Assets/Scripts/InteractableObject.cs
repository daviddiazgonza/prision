﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableObject : MonoBehaviour
{
    [HideInInspector]
    public string message = "INTERACT";

    public virtual void Interact() { }

    public void DisableObject()
    {
        this.transform.tag = "Untagged";
        this.GetComponent<Outline>().enabled = false;
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class InitialTrainScript : MonoBehaviour
{
    public Transform train;
    public GameObject tunelCover;
    public bool postRead;
    public AudioSource trainAudioSource;
    public AudioClip steamClip;

    private float speed;
    // Start is called before the first frame update
    void Start()
    {
        postRead = false;
        speed = 20;
    }

    // Update is called once per frame
    void Update()
    {

        train.transform.Translate(Vector3.right * Time.deltaTime * speed);

        if (train.transform.position.x >= -231)
        {
            if (!postRead)
            {
                train.transform.position = new Vector3(-705f, 35.5f, -119.345f);
            }
            else
            {
                if (train.transform.position.x >= -230f && train.transform.position.x <= -228f)
                {
                    train.transform.position = new Vector3(40f, 35.5f, -119.345f);
                }

                if (train.transform.position.x >= 54.87f)
                {
                    DOTween.To(() => trainAudioSource.pitch, x=> trainAudioSource.pitch = x, 0.5f, 5).OnComplete(PlaySteamSound);
                    speed = 5;
                }

                if (train.transform.position.x >= 84.87f)
                {
                    
                    this.gameObject.GetComponent<InitialTrainScript>().enabled = false;
                    GameManager.instance.player.GetComponent<UnityStandardAssets.Characters.FirstPerson.FirstPersonController>().m_WalkSpeed = 6;
                    GameManager.instance.player.GetComponent<AudioSource>().enabled = true;
                    GameManager.instance.player.transform.parent = null;

                    tunelCover.SetActive(true);
                }
            }
        }
    }

    void PlaySteamSound()
    {
        trainAudioSource.Pause();
        trainAudioSource.clip = steamClip;
        trainAudioSource.Play();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class InteractableDoor : InteractableObject
{
    public int id;
    public Transform target;
    public Vector3 finalRotation;
    public bool customSpeed;
    public float speed;
    

    private void Start()
    {
        message = "OPEN DOOR";
        if (!customSpeed) 
        {
            speed = 1.5f;
        }
    }

    public override void Interact()
    {
        if (Inventory.instance.door_keys[id] == "Unlocked")
        {
            
            target.DORotate(finalRotation, speed, RotateMode.Fast);
            if(id == 5)
            {
                AudioManager.Instance.Play("metal_door");
            }
            else
            {
                AudioManager.Instance.Play("door");
            }
            
            DisableObject();
        }
        else
        {
            AudioManager.Instance.Play("locked_door");
            DialogManager.instance.ShowDialog("It is closed. I need a key.", 3);
        }
    }
}

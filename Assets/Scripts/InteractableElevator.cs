﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class InteractableElevator : InteractableObject
{
    private bool finish = false;
    private bool start = false;
    public Transform elevator;
    public Transform elevatorEntryDoor;
    public Vector3 elevatorEntryDoorFinalPosition;
    public Transform elevatorExitDoor;
    public Vector3 elevatorExitDoorFinalPosition;
    public GameObject elevatorRope;
    public GameObject moon;

    private void Start()
    {
        message = "ACTIVATE ELEVATOR";
    }

    void Update()
    {
        if (start)
        {
            if (!finish)
            {
                elevator.Translate(Vector3.up * Time.deltaTime * 5);
                if (elevator.position.y >= 181.5)
                {
                    elevator.GetComponent<AudioSource>().enabled = false;
                    AudioManager.Instance.Play("gate");
                    elevatorExitDoor.DOLocalMove(elevatorExitDoorFinalPosition, 2f);
                    elevatorRope.SetActive(false);
                    finish = true;
                    start = false;
                    GameManager.instance.player.transform.parent = null;
                }
            }
        }
    }

    public override void Interact()
    {
        AudioManager.Instance.Play("gate");
        moon.transform.localPosition = new Vector3(-51.4f,45.9f,-161.1f);
        moon.transform.localScale = new Vector3(100, 100, 100);
        GameManager.instance.player.transform.parent = elevator;
        GameManager.instance.player.transform.GetChild(0).GetComponent<Camera>().farClipPlane = 100;
        GameManager.instance.player.transform.GetChild(0).GetChild(0).GetComponent<Camera>().farClipPlane = 100;
        this.transform.GetComponent<Animator>().SetTrigger("Activate");
        elevator.GetComponent<AudioSource>().enabled = true;
        elevatorEntryDoor.DOLocalMove(elevatorEntryDoorFinalPosition, 2f);
        start = true;
        DisableObject();
    }
}

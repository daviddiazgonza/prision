﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerInteraction : MonoBehaviour
{
    public GameObject interact_canvas_keyboard;
    public GameObject interact_canvas_controller;
    private GameObject interact_canvas;

    private Transform last_hit = null;


    private void Start()
    {
        if (Input.GetJoystickNames().Length > 0 && Input.GetJoystickNames()[0] != "")
        {
            //interact_canvas_controller.SetActive(true);
            interact_canvas = interact_canvas_controller;
        }
        else
        {
            //interact_canvas_keyboard.SetActive(true);
            interact_canvas = interact_canvas_keyboard;
        }
    }
    void FixedUpdate()
    {
        int layerMask = 1 << 11;

        layerMask = ~layerMask;

        RaycastHit hit;
        // Does the ray intersect any objects excluding the player layer
        if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, 2.5f, layerMask))
        {
            if(hit.transform.tag == "interactable")
            {
                last_hit = hit.transform;
                interact_canvas.transform.GetChild(0).GetComponent<Text>().text = hit.transform.GetComponent<InteractableObject>().message;
                interact_canvas.SetActive(true);
                ChangeColor( new Color(0.08490568f, 0.07729621f, 0.08449854f), hit.transform);
               // hit.transform.GetComponent<MeshRenderer>().material.color = new Color(0.08490568f, 0.07729621f, 0.08449854f);

                if ((Input.GetKeyDown(KeyCode.E) || Input.GetButtonDown("A Button")))
                {
                    hit.transform.GetComponent<InteractableObject>().Interact();
                }
            }
            else
            {
                interact_canvas.SetActive(false);
                if (last_hit != null)
                {
                    ChangeColor(Color.black, last_hit);
                    //last_hit.GetComponent<MeshRenderer>().material.color = Color.black;
                }
            }
            
        }
        else
        {
            interact_canvas.SetActive(false);
            if (last_hit != null)
            {
                ChangeColor(Color.black, last_hit);
                //last_hit.GetComponent<MeshRenderer>().material.color = Color.black;
            }
        }
    }

    private void Update()
    {
        if (Input.GetJoystickNames().Length > 0 && Input.GetJoystickNames()[0] != "")
        {
            //interact_canvas_controller.SetActive(true);
            interact_canvas = interact_canvas_controller;
        }
        else
        {
            //interact_canvas_keyboard.SetActive(true);
            interact_canvas = interact_canvas_keyboard;
        }
    }

    private void ChangeColor(Color color, Transform hit_object)
    {
        if (hit_object.GetComponent<MeshRenderer>() != null)
        {
            hit_object.GetComponent<MeshRenderer>().material.color = color;
        }
        if(hit_object.childCount > 0)
        {
            for(int i = 0; i < hit_object.childCount; i++)
            {
                if (hit_object.GetChild(i).GetComponent<MeshRenderer>() != null)
                {
                    hit_object.GetChild(i).GetComponent<MeshRenderer>().material.color = color;
                }
            }
        }
    }
}

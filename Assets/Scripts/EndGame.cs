﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class EndGame : MonoBehaviour
{
    public Transform startText;
    public MainMenuScript mainMenuScript;

    public void EndAnimation()
    {
        startText.gameObject.SetActive(true);
        mainMenuScript.enabled = true;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoatMovement : MonoBehaviour
{
    private float speed = 1;
    private float amplitude = 0.002f;


    void Start()
    {
        
    }

    void Update()
    {

        float y0 = transform.position.y;
        float r0 = transform.rotation.z;

        // Put the floating movement in the Update function:

        //transform.position.y = y0 + amplitude * Mathf.Sin(speed * Time.time);

        transform.position = new Vector3(transform.position.x, y0 + amplitude * Mathf.Sin(speed * Time.time), transform.position.z);
        //transform.rotation = Quaternion.EulerAngles( new Vector3(transform.rotation.x, -44.209f, r0 + 0.02f * Mathf.Sin(speed * Time.time)));
        transform.Rotate(Vector3.forward * 0.02f * Mathf.Sin(speed * Time.time));
        //initialValue += Time.deltaTime *10;
        //this.transform.Rotate(new Vector3(this.transform.rotation.x, this.transform.rotation.y * Mathf.Sin(initialValue), this.transform.rotation.z ));
    }
}

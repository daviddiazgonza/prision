﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.SceneManagement;

public class InteractableCorpse : InteractableObject
{

    public GameObject player;
    public GameObject table;
    public GameObject plane;
    public GameObject finalScenePoint;
    public GameObject laser;
    public GameObject laserEffect;
    public GameObject badGuy;

    private void Start()
    {
        message = "REMOVE BLANKET";
    }

    public override void Interact()
    {
        DisableObject();
        player.GetComponent<UnityStandardAssets.Characters.FirstPerson.FirstPersonController>().enabled = false;
        AudioManager.Instance.Play("door");
        Sequence firstSequence = DOTween.Sequence();
        firstSequence.Append(finalScenePoint.transform.DORotate(new Vector3(0, 0, 0), 2).OnComplete(PlayHitSound));
        //suena puerta
        //suena golpe

        Sequence mySequence = DOTween.Sequence();
        mySequence.PrependInterval(4);
        mySequence.Append(finalScenePoint.transform.DORotate(new Vector3(0, 0, 0), 2f).OnComplete(OpenEyes));
        mySequence.Append(finalScenePoint.transform.DORotate(new Vector3(0, 0, 0), 3).OnComplete(PlayBedSound));
        //sonido camilla
        mySequence.Append(finalScenePoint.transform.DORotate(new Vector3(0, 0, -90), 13));
        mySequence.Append(finalScenePoint.transform.DORotate(new Vector3(0, 0, -90), 2).OnComplete(PlayLaserSound));
        //sonido carga laser
        mySequence.Append(laser.transform.GetComponent<MeshRenderer>().material.DOColor(Color.white, 5).OnComplete(ActivateLaserEffect));
        mySequence.Append(finalScenePoint.transform.DORotate(new Vector3(0, 0, -90), 3).OnComplete(EndGame));
    }

    public void OpenEyes()
    {
        plane.SetActive(false);
    }

    public void ActivateLaserEffect()
    {
        //sonido laser
        laserEffect.SetActive(true);
    }

    public void EndGame()
    {
        AudioManager.Instance.Pause();
        AudioManager.Instance.PauseMusic();
        AudioManager.Instance.Play("laser_shot");
        plane.GetComponent<MeshRenderer>().material.color = Color.white;
        plane.SetActive(true);
        plane.transform.GetComponent<MeshRenderer>().material.DOColor(Color.black, 3).OnComplete(RestartGame);
    }

    public void RestartGame()
    {
        SceneManager.LoadScene(2);
    }

    public void PlayBedSound()
    {
        AudioManager.Instance.Play("crane");
    }

    public void PlayLaserSound()
    {
        AudioManager.Instance.Play("electricity");
        AudioManager.Instance.PlayMusic();
    }

    public void PlayHitSound()
    {
        AudioManager.Instance.Play("punch");
        TeleportPlayer();
    }

    public void TeleportPlayer()
    {
        plane.SetActive(true);
        table.SetActive(false);
        player.transform.position = finalScenePoint.transform.position;
        player.transform.rotation = Quaternion.Euler(new Vector3(-18, -90, 0));
        player.transform.parent = finalScenePoint.transform;
        badGuy.SetActive(true);
    }
}

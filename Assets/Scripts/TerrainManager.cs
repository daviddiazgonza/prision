﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainManager : MonoBehaviour
{
    public GameObject[] terrainList;

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.name == "Terrain_Trigger_1")
        {
            terrainList[0].SetActive(true);
            terrainList[5].SetActive(true);
            terrainList[7].SetActive(true);
        }

        if (other.gameObject.name == "Terrain_Trigger_2")
        {
            terrainList[1].SetActive(true);
            terrainList[8].SetActive(false);
          
        }

        if (other.gameObject.name == "Terrain_Trigger_3")
        {
            terrainList[0].SetActive(false);
            terrainList[3].SetActive(true);

        }

        if (other.gameObject.name == "Terrain_Trigger_4")
        {
            terrainList[5].SetActive(false);
            terrainList[7].SetActive(false);

        }

        if (other.gameObject.name == "Terrain_Trigger_5")
        {
            terrainList[9].SetActive(true);
            terrainList[1].SetActive(false);

        }
    }
}

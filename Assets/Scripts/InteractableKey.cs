﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableKey : InteractableObject
{
    public int id;

    private void Start()
    {
        message = "PICK KEY";
    }

    public override void Interact()
    {
        AudioManager.Instance.Play("key");
        DisableObject();
        Inventory.instance.door_keys[id] = "Unlocked";
        DialogManager.instance.ShowDialog("Mmm... I wonder where this key is from", 3);
        Destroy(this.gameObject);
    }
}
